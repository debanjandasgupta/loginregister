const expr = require('express');
const sequelize = require('../db/db');
const user = require('../models/users');
const reg = require('../controllers/register')
let router = expr.Router();


router.route("/firstname/:firstname/lastname/:lastname/email/:email/password/:password")
.post(reg.registrationMaker)

router.route('')
.get(reg.registrationChecker)

module.exports = router;