const expr = require('express');

const loginChecker = require('../controllers/login')
let router = expr.Router();


router.route("/email/:email/password/:password")
.post(loginChecker.loginMaker);

router.route('')
.get(loginChecker.loginChecker);

module.exports = router;