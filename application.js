const expr = require('express')
const reg = require('./routes/register')
const login = require('./routes/login')
const app = expr()

app.use("/register", reg);
app.use("/login", login);

app.route('/', (req, res) => {
    res.send("Hello from the application.")
})

const port = process.env.port || 3000;
app.listen(port, () => {
    console.log(`Listening to port: ${port}..`)
})