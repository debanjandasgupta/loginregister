const sequelize = require('../db/db');
const user = require('../models/users');


exports.loginMaker = (req, res) => {
    user.findOne({
        where: {
            email: req.params.email,
            password: req.params.password
        },
        attributes: ['id', 'first_name', 'last_name']
    }).then((users) => {
        let s = {
            status_code: 200,
            id: users.id,
            fullname: users.first_name + " " + users.last_name
        };
        res.send(s);
    });
};


exports.loginChecker = (req, res) => {
    res.send({
        status_code: 203,
        message: "Enter the parameters."
    });
};