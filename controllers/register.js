const sequelize = require('../db/db');
const user = require('../models/users');


exports.registrationMaker = async (req, res) => {
    let count = await user.count({
        where: {
            email: req.params.email
        }
    });
    if (count > 0) {
        let s = {
            status_code: 204,
            message: `Email already exists.`
        };
        res.send(s);
    }
    else {
        user.create({
            first_name: req.params.firstname,
            last_name: req.params.lastname,
            email: req.params.email,
            password: req.params.password
        }).then(() => {
            let s = {
                status_code: 200,
                message: `welcome ${req.params.firstname} ${req.params.lastname}`
            };
            res.send(s);
        })
    }
};


exports.registrationChecker = (req, res) => {
    res.send({
        status_code: 203,
        message: "Enter the parameters."
    });
};